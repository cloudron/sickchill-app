#!/bin/bash

set -eu

[[ ! -f /app/data/sickchill.ini ]] && cp /app/pkg/sickchill.ini /app/data/sickchill.ini

mkdir -p /app/data/sickchill /app/data/sickchill_cache /app/data/TVShows

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Starting SickChill"
exec /usr/local/bin/gosu cloudron:cloudron python3 /app/code/sickchill/SickChill.py --nolaunch --datadir=/app/data/sickchill --config=/app/data/sickchill.ini
