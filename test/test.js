#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    assert = require('assert'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const username = process.env.TEST_USERNAME || process.env.USERNAME;
const password = process.env.TEST_PASSWORD || process.env.PASSWORD;

if (!username || !password) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    let browser, app;

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    before(() => {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1920, height: 1024 })).build();
    });

    after(() => {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0];
        expect(app).to.be.an('object');
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await browser.wait(until.elementLocated(By.className('navbar-brand')), TIMEOUT);
    }

    async function addShowDirectory() {
        await browser.get(`https://${app.fqdn}/config/general/`);
        await browser.wait(until.elementLocated(By.id('addRootDir')), TIMEOUT);
        await browser.findElement(By.id('addRootDir')).click();
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//div[@class="fileBrowserFieldContainer"]//input[@type="text" and contains(@class, "fileBrowserField")]')).sendKeys('/app/data/TVShows');
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//div[@class="fileBrowserFieldContainer"]//input[@type="text" and contains(@class, "fileBrowserField")]')).sendKeys(Key.ENTER);
        await browser.sleep(3000);
        await browser.findElement(By.css('button.ui-button:nth-child(1)')).click();
        await browser.findElement(By.css('#rootDirs option[value*="TVShows"]'));
        await browser.sleep(3000);
    }

    async function addShow() {
        await browser.get(`https://${app.fqdn}/addShows/newShow/`);
        await browser.wait(until.elementLocated(By.id('show-name')), TIMEOUT);
        await browser.findElement(By.id('show-name')).sendKeys('Friends');
        await browser.findElement(By.id('search-button')).click();
        await browser.sleep(3000);
        await browser.wait(until.elementLocated(By.css('input[value*="id=|79168|Friends|1994-09-22|false"]')), TIMEOUT);
        await browser.findElement(By.css('input[value*="id=|79168|Friends|1994-09-22|false"]')).click();
        await browser.findElement(By.id('addShowButton')).click();
        console.log('Waiting for 10 seconds for show to appear');
        await browser.sleep(10000);
    }

    async function isShowThere() {
        await browser.get(`https://${app.fqdn}/home/`);
        await browser.sleep(3000);
        await browser.findElement(By.css('.show-container[data-name*=friends]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.sleep(2000); // wait for "connection lost" error
        await browser.get(`https://${app.fqdn}/login`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
    }

    const sleep = (millis) => new Promise(resolve => setTimeout(resolve, millis));

    xit('build app', () => { execSync('cloudron build', EXEC_ARGS); });

    it('install app', () => { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('add shows directory', addShowDirectory);
    it('add show', addShow);
    it('is show there', isShowThere);
    it('can logout', logout);

    it('can restart app', () => { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('is show there', isShowThere);
    it('can logout', logout);

    it('backup app', () => { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async () => {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can login', login);
    it('is show there', isShowThere);
    it('can logout', logout);

    it('move to different location', () => { execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('is show there', isShowThere);
    it('can logout', logout);

    it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install app', () => { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('add shows directory', addShowDirectory);
    it('add show', addShow);
    it('can logout', logout);
    it('can update', () => { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('is show there', isShowThere);
    it('can logout', logout);
    it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });
});
