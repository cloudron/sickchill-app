FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/sickchill /app/code/.cache/pypoetry

# force install libssl1.1
RUN echo "deb http://security.ubuntu.com/ubuntu focal-security main" | sudo tee /etc/apt/sources.list.d/focal-security.list
RUN apt-get update -qq && \
    apt-get install -yqq libxslt1.1 libssl1.1 libmediainfo0v5 mediainfo unrar python3.10-venv python3-distutils-extra libmediainfo-dev && \
    rm -rf /var/lib/apt/lists/*

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sed 's#/proc/self/exe#/bin/sh#g' | sh -s -- -y --profile minimal --default-toolchain nightly

# Setting the encoding for python, otherwise we get weird errors
ENV PYTHONIOENCODING="UTF-8"
ENV PYTHONUNBUFFERED=1

ENV POETRY_INSTALLER_PARALLEL=false
ENV POETRY_VIRTUALENVS_CREATE=false
ENV POETRY_VIRTUALENVS_IN_PROJECT=false
ENV POETRY_CACHE_DIR="/app/code/.cache/pypoetry"

ENV POETRY_HOME="/app/code/.poetry"
ENV CARGO_HOME="/app/code/.cargo"
ENV RUSTUP_HOME="/app/code/.rustup"

ENV PATH="$RUSTUP_HOME/bin:$CARGO_HOME/bin:/app/code/venv/local/bin:$POETRY_VIRTUALENVS_PATH/bin:$PATH"

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ENV RUSTUP_PERMIT_COPY_RENAME="yes"
ENV RUSTUP_IO_THREADS=1
ENV CARGO="$CARGO_HOME/bin/cargo"

ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PIP_EXTRA_INDEX_URL="https://www.piwheels.org/simple"

RUN python3 -m venv /app/code/venv --upgrade --upgrade-deps && \
    pip install -U wheel setuptools-rust

WORKDIR /app/code/sickchill

# renovate: datasource=github-releases depName=SickChill/sickchill versioning=loose
ARG SICKCHILL_VERSION=2024.2.27

RUN curl -L "https://github.com/SickChill/sickchill/archive/refs/tags/${SICKCHILL_VERSION}.tar.gz" | tar -xz --strip-components=1 -C /app/code/sickchill

RUN pip install --upgrade poetry && \
    poetry run pip install -U setuptools-rust pycparser && \
    poetry build --no-interaction --no-ansi && \
    pip install --upgrade --find-links=./dist "sickchill[speedups]"

RUN mkdir  /app/code/sickchill-wheels && \
    pip download sickchill --dest /app/code/sickchill-wheels && \
    rm -rf /app/code/sickchill-wheels/*none-any.whl && \
    rm -rf /app/code/sickchill-wheels/*.gz;

# to hide "SickChill no longer is supported unless installed with pip or poetry. Source and git installs are for experienced users only"  warning
RUN rm /app/code/sickchill/pyproject.toml

COPY sickchill.ini start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
